# Hadoop-WordCount

#### BIG DATA Postgraduation Course
#### Universidade Positivo - Curitiba - Brazil
#### By André Silva Alves 
##### @andresilvaalves


## commands to execute Java program

###### -- Create directories in the HDFS

```
$ hadoop fs -mkdir wordcount 
$ hadoop fs -mkdir wordcount/input
```
###### --copy files to directory

```
$ hadoop fs -put SpaceX.txt wordcount/input/
```
##### -- command to execute java program

```
$ hadoop jar WordCount.jar wordcount/input wordcount/output
```
###### -- reading results and copying to one text file

```
$ hadoop fs -cat wordcount/output/* > result.txt
$ cat result.txt
```

###### --deleting directory

```
$ hadoop fs -rm -r wordcount/output
```

## Alternatives

#### WordCount Spark


https://gist.github.com/andresilvaalves/78f742dbb3dffd7f36945a6d81f46702

#### WordCount Spark SQL

https://gist.github.com/andresilvaalves/f669df056c63e03a8c76aab6991cc154#file-wordcount-sparksql


