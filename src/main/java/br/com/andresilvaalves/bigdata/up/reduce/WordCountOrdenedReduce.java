package br.com.andresilvaalves.bigdata.up.reduce;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WordCountOrdenedReduce extends Reducer<Text, Text, Text, Text> {

	@Override
	protected void reduce(Text Key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		context.write(values.iterator().next(), new Text());
	}
}
