package br.com.andresilvaalves.bigdata.up;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import br.com.andresilvaalves.bigdata.up.map.WordCountMap;
import br.com.andresilvaalves.bigdata.up.map.WordCountOrdenedMap;
import br.com.andresilvaalves.bigdata.up.reduce.WordCountOrdenedReduce;
import br.com.andresilvaalves.bigdata.up.reduce.WordCountReduce;
import br.com.andresilvaalves.bigdata.up.util.SortByKeydesc;

public class WordCount extends Configured implements Tool {
	private static final Logger LOG = Logger.getLogger(WordCount.class);
	
	private static /*final*/ String PATH_INPUT  = "";
	private static /*final*/ String PATH_OUTPUT = "";
	private static /*final*/ String PATH_OUTPUT_TEMP = "";

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new WordCount(), args);
		LOG.info("===================");
		LOG.info("WordCount finished with success!");
		LOG.info("Good Job!");
		LOG.info("====================");
		System.exit(res);
	}

	public int run(String[] args) throws Exception {
		LOG.info("========================================");
		LOG.info("WordCount Ordened - By: Andre Silva Alves.");
		LOG.info("========================================");
		
		if (args.length < 2) {
			LOG.error("HOUSTON, We have a problem!!");
			LOG.error("Please Enter the input and output parameters");
			System.exit(-1);
		}
		PATH_INPUT  = args[0];
		if(args.length <3) {
			PATH_OUTPUT = args[1];
			PATH_OUTPUT_TEMP = PATH_OUTPUT+"_TEMP"; 
		}else {
			PATH_OUTPUT_TEMP = args[1];
			PATH_OUTPUT = args[2];
		}
		
		LOG.info("PATH_INPUT = "+PATH_INPUT);
		LOG.info("PATH_OUTPUT = "+PATH_OUTPUT);
		LOG.info("PATH_OUTPUT_TEMP = "+PATH_OUTPUT_TEMP);
		
		readFiles(PATH_INPUT);
		
		wordCount();
		
		wordCountOrdened();
		
		readFiles(PATH_OUTPUT);
		
		return 0;
	}
	
	private void wordCount() throws Exception  {
		LOG.info("=====================");
		LOG.info("executing WordCount!");
		LOG.info("=====================");
		
		Job job = Job.getInstance(getConf(), "wordcount");
		job.setJarByClass(this.getClass());
		
		FileInputFormat.addInputPath(job, new Path(PATH_INPUT));
		FileOutputFormat.setOutputPath(job, new Path(PATH_OUTPUT_TEMP));
		
		job.setMapperClass(WordCountMap.class);
		job.setReducerClass(WordCountReduce.class);
		job.setCombinerClass(WordCountReduce.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		job.waitForCompletion(true);
	}
	
	private void wordCountOrdened() throws Exception {
		LOG.info("===========================");
		LOG.info("Executing WordCount Ordened!");
		LOG.info("===========================");
		
		Job job = Job.getInstance(getConf(), "wordcount Ordened");
		job.setJarByClass(this.getClass());
		
		FileInputFormat.addInputPath(job, new Path(PATH_OUTPUT_TEMP));
		FileOutputFormat.setOutputPath(job, new Path(PATH_OUTPUT));
		
		job.setMapperClass(WordCountOrdenedMap.class);
		job.setReducerClass(WordCountOrdenedReduce.class);
		//job.setCombinerClass(WordCountReduce.class);
		
		job.setSortComparatorClass(SortByKeydesc.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.waitForCompletion(true);
	}
	
	private void readFiles(String path) throws Exception {
		LOG.info("=========================");
		LOG.info("Listing files on HDFS....");
		LOG.info("PATH : "+path);
		LOG.info("==========================");

		try {
			FileSystem fileSystem = FileSystem.get(getConf());

			RemoteIterator<LocatedFileStatus> listFiles = fileSystem.listFiles(new Path(path), false);

			while (listFiles.hasNext()) {
				LocatedFileStatus fileStatus = listFiles.next();
				LOG.info("File Name = " + fileStatus.getPath());
			}
		} catch (Exception e) {
			throw new Exception("HOUSTON, We have a problem!! "+e.getMessage(),e);
		}
		LOG.info("============================");
	}
	
}
